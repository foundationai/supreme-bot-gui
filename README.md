# README #

SUPREME BOT GUI Created By John Dogan

### What is this repository for? ###

* Supreme Bot created to purchase items from www.supremenewyork.com that sell out in under 5 seconds.
* 1.0
* Created with Python3 and PyQt4

### How do I get set up? ###

WINDOWS OS
--------------
- INSTALL EXE

OTHER OS
--------------
1. Install Python3 and PyQt4
2. Run python script with python 3
3. You are good to go!

* MUST HAVE PYTHON3 INSTALLED
* ENTER DATA THEN PRESS START.
* PRESS STOP TO STOP ALL RUNNING PROCESSES
* .EXE will be added soon.

### Who do I talk to? ###

* l0sm3n@gmail.com