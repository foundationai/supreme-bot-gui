'''
Author: John Dogan [www.thestreetplug.com]
Created With Python3 and PyQt
---------------------
SUPREME GUI
---------------------
This program takes user data and purchases items from www.supremenewyork.com
that sell out in under 5 seconds or less.
'''

import sys, json, time, requests, codecs
import urllib.request as urllib_request
import urllib.parse as urllib_parse
from datetime import datetime
from urllib.error import URLError
from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8


    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


'''
Class creates GUI and sets up program that can make instant purchases from the Supreme online store
Recommended with Windows, Linux servers
'''
class Ui_MainWindow(object):

    # GUI
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(742, 500)
        MainWindow.setMinimumSize(QtCore.QSize(742, 500))
        MainWindow.setMaximumSize(QtCore.QSize(800, 500))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("DejaVu Sans"))
        font.setPointSize(10)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        MainWindow.setFont(font)
        MainWindow.setStyleSheet(_fromUtf8("background-color: rgb(100, 100, 100);\n"
                                           "alternate-background-color: rgb(148, 148, 148);"))
        MainWindow.setTabShape(QtGui.QTabWidget.Triangular)
        MainWindow.setDockNestingEnabled(True)
        MainWindow.setUnifiedTitleAndToolBarOnMac(False)
        self.centralWidget = QtGui.QWidget(MainWindow)
        self.centralWidget.setObjectName(_fromUtf8("centralWidget"))
        self.loadButton = QtGui.QPushButton(self.centralWidget)
        self.loadButton.setGeometry(QtCore.QRect(30, 340, 111, 41))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("URW Gothic L"))
        font.setPointSize(12)
        font.setBold(False)
        font.setWeight(50)
        self.loadButton.setFont(font)
        self.loadButton.setObjectName(_fromUtf8("loadButton"))
        self.saveButton = QtGui.QPushButton(self.centralWidget)
        self.saveButton.setGeometry(QtCore.QRect(160, 340, 111, 41))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("URW Gothic L"))
        font.setPointSize(12)
        font.setBold(False)
        font.setWeight(50)
        self.saveButton.setFont(font)
        self.saveButton.setObjectName(_fromUtf8("saveButton"))
        self.consoleEdit = QtGui.QTextEdit(self.centralWidget)
        self.consoleEdit.setGeometry(QtCore.QRect(30, 390, 681, 81))
        self.consoleEdit.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.consoleEdit.setReadOnly(True)
        self.consoleEdit.setObjectName(_fromUtf8("consoleEdit"))
        self.firstNameLabel = QtGui.QLabel(self.centralWidget)
        self.firstNameLabel.setGeometry(QtCore.QRect(20, 30, 71, 21))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("URW Gothic L"))
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.firstNameLabel.setFont(font)
        self.firstNameLabel.setObjectName(_fromUtf8("firstNameLabel"))
        self.firstNameEdit = QtGui.QLineEdit(self.centralWidget)
        self.firstNameEdit.setGeometry(QtCore.QRect(100, 30, 81, 22))
        self.firstNameEdit.setObjectName(_fromUtf8("firstNameEdit"))
        self.lastNameEdit = QtGui.QLineEdit(self.centralWidget)
        self.lastNameEdit.setGeometry(QtCore.QRect(100, 70, 81, 22))
        self.lastNameEdit.setText(_fromUtf8(""))
        self.lastNameEdit.setObjectName(_fromUtf8("lastNameEdit"))
        self.lastNameLabel = QtGui.QLabel(self.centralWidget)
        self.lastNameLabel.setGeometry(QtCore.QRect(20, 70, 71, 21))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("URW Gothic L"))
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.lastNameLabel.setFont(font)
        self.lastNameLabel.setObjectName(_fromUtf8("lastNameLabel"))
        self.emailLineEdit = QtGui.QLineEdit(self.centralWidget)
        self.emailLineEdit.setGeometry(QtCore.QRect(100, 110, 91, 22))
        self.emailLineEdit.setText(_fromUtf8(""))
        self.emailLineEdit.setObjectName(_fromUtf8("emailLineEdit"))
        self.emailLabel = QtGui.QLabel(self.centralWidget)
        self.emailLabel.setGeometry(QtCore.QRect(30, 110, 51, 21))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("URW Gothic L"))
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.emailLabel.setFont(font)
        self.emailLabel.setObjectName(_fromUtf8("emailLabel"))
        self.phoneLineEdit = QtGui.QLineEdit(self.centralWidget)
        self.phoneLineEdit.setGeometry(QtCore.QRect(100, 150, 91, 22))
        self.phoneLineEdit.setText(_fromUtf8(""))
        self.phoneLineEdit.setObjectName(_fromUtf8("phoneLineEdit"))
        self.phoneLabel = QtGui.QLabel(self.centralWidget)
        self.phoneLabel.setGeometry(QtCore.QRect(20, 150, 61, 21))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("URW Gothic L"))
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.phoneLabel.setFont(font)
        self.phoneLabel.setObjectName(_fromUtf8("phoneLabel"))
        self.startButton = QtGui.QPushButton(self.centralWidget)
        self.startButton.setGeometry(QtCore.QRect(470, 340, 111, 41))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("URW Gothic L"))
        font.setPointSize(12)
        font.setBold(False)
        font.setWeight(50)
        self.startButton.setFont(font)
        self.startButton.setLayoutDirection(QtCore.Qt.LeftToRight)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("../../../Documents/SUPREME/go.png")), QtGui.QIcon.Normal,
                       QtGui.QIcon.Off)
        self.startButton.setIcon(icon)
        self.startButton.setIconSize(QtCore.QSize(12, 12))
        self.startButton.setShortcut(_fromUtf8(""))
        self.startButton.setObjectName(_fromUtf8("startButton"))
        self.stopButton = QtGui.QPushButton(self.centralWidget)
        self.stopButton.setGeometry(QtCore.QRect(600, 340, 111, 41))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("URW Gothic L"))
        font.setPointSize(12)
        font.setBold(False)
        font.setWeight(50)
        self.stopButton.setFont(font)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(_fromUtf8("../../../Documents/SUPREME/stop.png")), QtGui.QIcon.Normal,
                        QtGui.QIcon.Off)
        self.stopButton.setIcon(icon1)
        self.stopButton.setIconSize(QtCore.QSize(20, 20))
        self.stopButton.setObjectName(_fromUtf8("stopButton"))
        self.addressLabel = QtGui.QLabel(self.centralWidget)
        self.addressLabel.setGeometry(QtCore.QRect(20, 190, 71, 21))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("URW Gothic L"))
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.addressLabel.setFont(font)
        self.addressLabel.setObjectName(_fromUtf8("addressLabel"))
        self.addressLineEdit = QtGui.QLineEdit(self.centralWidget)
        self.addressLineEdit.setGeometry(QtCore.QRect(100, 190, 101, 22))
        self.addressLineEdit.setText(_fromUtf8(""))
        self.addressLineEdit.setObjectName(_fromUtf8("addressLineEdit"))
        self.zipLabel = QtGui.QLabel(self.centralWidget)
        self.zipLabel.setGeometry(QtCore.QRect(20, 230, 71, 21))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("URW Gothic L"))
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.zipLabel.setFont(font)
        self.zipLabel.setObjectName(_fromUtf8("zipLabel"))
        self.zipLineEdit = QtGui.QLineEdit(self.centralWidget)
        self.zipLineEdit.setGeometry(QtCore.QRect(100, 230, 71, 22))
        self.zipLineEdit.setText(_fromUtf8(""))
        self.zipLineEdit.setObjectName(_fromUtf8("zipLineEdit"))
        self.cityLineEdit = QtGui.QLineEdit(self.centralWidget)
        self.cityLineEdit.setGeometry(QtCore.QRect(100, 270, 91, 22))
        self.cityLineEdit.setText(_fromUtf8(""))
        self.cityLineEdit.setObjectName(_fromUtf8("cityLineEdit"))
        self.cityLabel = QtGui.QLabel(self.centralWidget)
        self.cityLabel.setGeometry(QtCore.QRect(30, 270, 41, 21))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("URW Gothic L"))
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.cityLabel.setFont(font)
        self.cityLabel.setObjectName(_fromUtf8("cityLabel"))
        self.countryLabel = QtGui.QLabel(self.centralWidget)
        self.countryLabel.setGeometry(QtCore.QRect(270, 30, 61, 21))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("URW Gothic L"))
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.countryLabel.setFont(font)
        self.countryLabel.setObjectName(_fromUtf8("countryLabel"))
        self.countryCombo = QtGui.QComboBox(self.centralWidget)
        self.countryCombo.setGeometry(QtCore.QRect(360, 30, 81, 22))
        self.countryCombo.setObjectName(_fromUtf8("countryCombo"))
        self.countryCombo.addItem(_fromUtf8(""))
        self.stateLabel = QtGui.QLabel(self.centralWidget)
        self.stateLabel.setGeometry(QtCore.QRect(280, 70, 51, 20))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("URW Gothic L"))
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.stateLabel.setFont(font)
        self.stateLabel.setObjectName(_fromUtf8("stateLabel"))
        self.stateCombo = QtGui.QComboBox(self.centralWidget)
        self.stateCombo.setGeometry(QtCore.QRect(360, 70, 51, 22))
        self.stateCombo.setMaxVisibleItems(4)
        self.stateCombo.setObjectName(_fromUtf8("stateCombo"))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.stateCombo.addItem(_fromUtf8(""))
        self.cardTypeLabel = QtGui.QLabel(self.centralWidget)
        self.cardTypeLabel.setGeometry(QtCore.QRect(260, 150, 81, 20))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("URW Gothic L"))
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.cardTypeLabel.setFont(font)
        self.cardTypeLabel.setObjectName(_fromUtf8("cardTypeLabel"))
        self.cardCombobox = QtGui.QComboBox(self.centralWidget)
        self.cardCombobox.setGeometry(QtCore.QRect(360, 150, 101, 22))
        self.cardCombobox.setObjectName(_fromUtf8("cardCombobox"))
        self.cardCombobox.addItem(_fromUtf8(""))
        self.cardCombobox.addItem(_fromUtf8(""))
        self.cardCombobox.addItem(_fromUtf8(""))
        self.cardNumberLabel = QtGui.QLabel(self.centralWidget)
        self.cardNumberLabel.setGeometry(QtCore.QRect(260, 190, 91, 20))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("URW Gothic L"))
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.cardNumberLabel.setFont(font)
        self.cardNumberLabel.setObjectName(_fromUtf8("cardNumberLabel"))
        self.cardLineEdit = QtGui.QLineEdit(self.centralWidget)
        self.cardLineEdit.setGeometry(QtCore.QRect(360, 190, 121, 22))
        self.cardLineEdit.setText(_fromUtf8(""))
        self.cardLineEdit.setObjectName(_fromUtf8("cardLineEdit"))
        self.expDateLabel = QtGui.QLabel(self.centralWidget)
        self.expDateLabel.setGeometry(QtCore.QRect(250, 230, 101, 20))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("URW Gothic L"))
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.expDateLabel.setFont(font)
        self.expDateLabel.setObjectName(_fromUtf8("expDateLabel"))
        self.expDateMonth = QtGui.QSpinBox(self.centralWidget)
        self.expDateMonth.setGeometry(QtCore.QRect(360, 230, 41, 23))
        self.expDateMonth.setMinimum(1)
        self.expDateMonth.setMaximum(12)
        self.expDateMonth.setObjectName(_fromUtf8("expDateMonth"))
        self.expDateYear = QtGui.QSpinBox(self.centralWidget)
        self.expDateYear.setGeometry(QtCore.QRect(410, 230, 61, 23))
        self.expDateYear.setMinimum(2014)
        self.expDateYear.setMaximum(2021)
        self.expDateYear.setObjectName(_fromUtf8("expDateYear"))
        self.cvvLabel = QtGui.QLabel(self.centralWidget)
        self.cvvLabel.setGeometry(QtCore.QRect(280, 270, 41, 21))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("URW Gothic L"))
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.cvvLabel.setFont(font)
        self.cvvLabel.setObjectName(_fromUtf8("cvvLabel"))
        self.cvvLineEdit = QtGui.QLineEdit(self.centralWidget)
        self.cvvLineEdit.setGeometry(QtCore.QRect(360, 270, 51, 21))
        self.cvvLineEdit.setText(_fromUtf8(""))
        self.cvvLineEdit.setObjectName(_fromUtf8("cvvLineEdit"))
        self.line = QtGui.QFrame(self.centralWidget)
        self.line.setGeometry(QtCore.QRect(230, 20, 21, 301))
        self.line.setFrameShape(QtGui.QFrame.VLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.line_2 = QtGui.QFrame(self.centralWidget)
        self.line_2.setGeometry(QtCore.QRect(493, 10, 20, 301))
        self.line_2.setFrameShape(QtGui.QFrame.VLine)
        self.line_2.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_2.setObjectName(_fromUtf8("line_2"))
        self.line_4 = QtGui.QFrame(self.centralWidget)
        self.line_4.setGeometry(QtCore.QRect(240, 0, 511, 21))
        self.line_4.setFrameShape(QtGui.QFrame.HLine)
        self.line_4.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_4.setObjectName(_fromUtf8("line_4"))
        self.line_5 = QtGui.QFrame(self.centralWidget)
        self.line_5.setGeometry(QtCore.QRect(0, 0, 261, 21))
        self.line_5.setFrameShape(QtGui.QFrame.HLine)
        self.line_5.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_5.setObjectName(_fromUtf8("line_5"))
        self.typeLabel = QtGui.QLabel(self.centralWidget)
        self.typeLabel.setGeometry(QtCore.QRect(530, 150, 71, 21))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("URW Gothic L"))
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.typeLabel.setFont(font)
        self.typeLabel.setObjectName(_fromUtf8("typeLabel"))
        self.colorLineEdit = QtGui.QLineEdit(self.centralWidget)
        self.colorLineEdit.setGeometry(QtCore.QRect(610, 70, 81, 22))
        self.colorLineEdit.setText(_fromUtf8(""))
        self.colorLineEdit.setObjectName(_fromUtf8("colorLineEdit"))
        self.colorLabel = QtGui.QLabel(self.centralWidget)
        self.colorLabel.setGeometry(QtCore.QRect(540, 70, 41, 21))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("URW Gothic L"))
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.colorLabel.setFont(font)
        self.colorLabel.setObjectName(_fromUtf8("colorLabel"))
        self.keywordLineEdit = QtGui.QLineEdit(self.centralWidget)
        self.keywordLineEdit.setGeometry(QtCore.QRect(610, 30, 81, 22))
        self.keywordLineEdit.setObjectName(_fromUtf8("keywordLineEdit"))
        self.keywordLabel = QtGui.QLabel(self.centralWidget)
        self.keywordLabel.setGeometry(QtCore.QRect(530, 30, 71, 21))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("URW Gothic L"))
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.keywordLabel.setFont(font)
        self.keywordLabel.setObjectName(_fromUtf8("keywordLabel"))
        self.sizeLabel = QtGui.QLabel(self.centralWidget)
        self.sizeLabel.setGeometry(QtCore.QRect(530, 110, 71, 21))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("URW Gothic L"))
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.sizeLabel.setFont(font)
        self.sizeLabel.setObjectName(_fromUtf8("sizeLabel"))
        self.sizeCombo = QtGui.QComboBox(self.centralWidget)
        self.sizeCombo.setGeometry(QtCore.QRect(610, 110, 91, 22))
        self.sizeCombo.setObjectName(_fromUtf8("sizeCombo"))
        self.sizeCombo.addItem(_fromUtf8(""))
        self.sizeCombo.addItem(_fromUtf8(""))
        self.sizeCombo.addItem(_fromUtf8(""))
        self.sizeCombo.addItem(_fromUtf8(""))
        self.typeCombo = QtGui.QComboBox(self.centralWidget)
        self.typeCombo.setGeometry(QtCore.QRect(610, 150, 101, 22))
        self.typeCombo.setObjectName(_fromUtf8("typeCombo"))
        self.typeCombo.addItem(_fromUtf8(""))
        self.typeCombo.addItem(_fromUtf8(""))
        self.typeCombo.addItem(_fromUtf8(""))
        self.typeCombo.addItem(_fromUtf8(""))
        self.typeCombo.addItem(_fromUtf8(""))
        self.typeCombo.addItem(_fromUtf8(""))
        self.typeCombo.addItem(_fromUtf8(""))
        self.typeCombo.addItem(_fromUtf8(""))
        self.typeCombo.addItem(_fromUtf8(""))
        self.typeCombo.addItem(_fromUtf8(""))
        self.typeCombo.addItem(_fromUtf8(""))
        self.typeCombo.addItem(_fromUtf8(""))
        self.delayLabel = QtGui.QLabel(self.centralWidget)
        self.delayLabel.setGeometry(QtCore.QRect(540, 270, 61, 21))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("URW Gothic L"))
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.delayLabel.setFont(font)
        self.delayLabel.setObjectName(_fromUtf8("delayLabel"))
        self.delayLineEdit = QtGui.QLineEdit(self.centralWidget)
        self.delayLineEdit.setGeometry(QtCore.QRect(610, 270, 41, 22))
        self.delayLineEdit.setObjectName(_fromUtf8("delayLineEdit"))
        self.line_6 = QtGui.QFrame(self.centralWidget)
        self.line_6.setGeometry(QtCore.QRect(510, 190, 221, 21))
        self.line_6.setFrameShape(QtGui.QFrame.HLine)
        self.line_6.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_6.setObjectName(_fromUtf8("line_6"))
        self.line_7 = QtGui.QFrame(self.centralWidget)
        self.line_7.setGeometry(QtCore.QRect(250, 100, 241, 41))
        self.line_7.setFrameShape(QtGui.QFrame.HLine)
        self.line_7.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_7.setObjectName(_fromUtf8("line_7"))
        self.line_3 = QtGui.QFrame(self.centralWidget)
        self.line_3.setGeometry(QtCore.QRect(0, 310, 751, 20))
        self.line_3.setFrameShape(QtGui.QFrame.HLine)
        self.line_3.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_3.setObjectName(_fromUtf8("line_3"))
        self.proxyLineEdit = QtGui.QLineEdit(self.centralWidget)
        self.proxyLineEdit.setGeometry(QtCore.QRect(610, 230, 111, 22))
        self.proxyLineEdit.setObjectName(_fromUtf8("proxyLineEdit"))
        self.proxyLabel = QtGui.QLabel(self.centralWidget)
        self.proxyLabel.setGeometry(QtCore.QRect(540, 230, 61, 21))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("URW Gothic L"))
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.proxyLabel.setFont(font)
        self.proxyLabel.setObjectName(_fromUtf8("proxyLabel"))
        MainWindow.setCentralWidget(self.centralWidget)
        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        self.startButton.setObjectName(_fromUtf8("startButton"))

        ############### BUTTON CLICKED EVENT STOP ##################
        self.stopButton.clicked.connect(self.stopButtonClick)
        #####################################################

        ############### BUTTON CLICKED EVENT START #################
        self.startButton.clicked.connect(self.startButtonClick)
        #####################################################

        ############## BUTTON CLICKED EVENT SAVE ##################
        self.saveButton.clicked.connect(self.saveButtonClick)
        #####################################################

        ############### BUTTON CLICKED EVENT LOAD #################
        self.loadButton.clicked.connect(self.loadButtonClick)
        #####################################################

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "SUPREME BOT", None))
        self.loadButton.setText(_translate("MainWindow", "LOAD", None))
        self.saveButton.setText(_translate("MainWindow", "SAVE", None))
        self.consoleEdit.setHtml(_translate("MainWindow",
                                            "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
                                            "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
                                            "p, li { white-space: pre-wrap; }\n"
                                            "</style></head><body style=\" font-family:\'Sans Serif\'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
                                            "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>",
                                            None))
        self.firstNameLabel.setText(_translate("MainWindow", "First Name", None))
        self.lastNameLabel.setText(_translate("MainWindow", "Last Name", None))
        self.emailLabel.setText(_translate("MainWindow", "E-Mail", None))
        self.phoneLabel.setText(_translate("MainWindow", "  Phone", None))
        self.startButton.setText(_translate("MainWindow", "START", None))
        self.stopButton.setText(_translate("MainWindow", "STOP", None))
        self.addressLabel.setText(_translate("MainWindow", " Address", None))
        self.zipLabel.setText(_translate("MainWindow", " ZIP Code", None))
        self.cityLabel.setText(_translate("MainWindow", "  City", None))
        self.countryLabel.setText(_translate("MainWindow", "Country", None))
        self.countryCombo.setItemText(0, _translate("MainWindow", "USA", None))
        self.stateLabel.setText(_translate("MainWindow", "State", None))
        self.stateCombo.setItemText(0, _translate("MainWindow", "AL", None))
        self.stateCombo.setItemText(1, _translate("MainWindow", "AK", None))
        self.stateCombo.setItemText(2, _translate("MainWindow", "AZ", None))
        self.stateCombo.setItemText(3, _translate("MainWindow", "AR", None))
        self.stateCombo.setItemText(4, _translate("MainWindow", "CA", None))
        self.stateCombo.setItemText(5, _translate("MainWindow", "CO", None))
        self.stateCombo.setItemText(6, _translate("MainWindow", "CT", None))
        self.stateCombo.setItemText(7, _translate("MainWindow", "DE", None))
        self.stateCombo.setItemText(8, _translate("MainWindow", "FL", None))
        self.stateCombo.setItemText(9, _translate("MainWindow", "GA", None))
        self.stateCombo.setItemText(10, _translate("MainWindow", "HI", None))
        self.stateCombo.setItemText(11, _translate("MainWindow", "ID", None))
        self.stateCombo.setItemText(12, _translate("MainWindow", "IL", None))
        self.stateCombo.setItemText(13, _translate("MainWindow", "IN", None))
        self.stateCombo.setItemText(14, _translate("MainWindow", "IA", None))
        self.stateCombo.setItemText(15, _translate("MainWindow", "KS", None))
        self.stateCombo.setItemText(16, _translate("MainWindow", "KY", None))
        self.stateCombo.setItemText(17, _translate("MainWindow", "LA", None))
        self.stateCombo.setItemText(18, _translate("MainWindow", "MI", None))
        self.stateCombo.setItemText(19, _translate("MainWindow", "MN", None))
        self.stateCombo.setItemText(20, _translate("MainWindow", "MS", None))
        self.stateCombo.setItemText(21, _translate("MainWindow", "MO", None))
        self.stateCombo.setItemText(22, _translate("MainWindow", "MT", None))
        self.stateCombo.setItemText(23, _translate("MainWindow", "NE", None))
        self.stateCombo.setItemText(24, _translate("MainWindow", "NH", None))
        self.stateCombo.setItemText(25, _translate("MainWindow", "NJ", None))
        self.stateCombo.setItemText(26, _translate("MainWindow", "NM", None))
        self.stateCombo.setItemText(27, _translate("MainWindow", "NY", None))
        self.stateCombo.setItemText(28, _translate("MainWindow", "NC", None))
        self.stateCombo.setItemText(29, _translate("MainWindow", "OD", None))
        self.stateCombo.setItemText(30, _translate("MainWindow", "OH", None))
        self.stateCombo.setItemText(31, _translate("MainWindow", "OK", None))
        self.stateCombo.setItemText(32, _translate("MainWindow", "OR", None))
        self.stateCombo.setItemText(33, _translate("MainWindow", "PA", None))
        self.stateCombo.setItemText(34, _translate("MainWindow", "RI", None))
        self.stateCombo.setItemText(35, _translate("MainWindow", "SC", None))
        self.stateCombo.setItemText(36, _translate("MainWindow", "SD", None))
        self.stateCombo.setItemText(37, _translate("MainWindow", "TN", None))
        self.stateCombo.setItemText(38, _translate("MainWindow", "TX", None))
        self.stateCombo.setItemText(39, _translate("MainWindow", "UT", None))
        self.stateCombo.setItemText(40, _translate("MainWindow", "VT", None))
        self.stateCombo.setItemText(41, _translate("MainWindow", "VA", None))
        self.stateCombo.setItemText(42, _translate("MainWindow", "WA", None))
        self.stateCombo.setItemText(43, _translate("MainWindow", "WV", None))
        self.stateCombo.setItemText(44, _translate("MainWindow", "WI", None))
        self.stateCombo.setItemText(45, _translate("MainWindow", "WY", None))
        self.cardTypeLabel.setText(_translate("MainWindow", " Card Type", None))
        self.cardCombobox.setItemText(0, _translate("MainWindow", "Visa", None))
        self.cardCombobox.setItemText(1, _translate("MainWindow", "Mastercard", None))
        self.cardCombobox.setItemText(2, _translate("MainWindow", "AmericanExpress", None))
        self.cardNumberLabel.setText(_translate("MainWindow", "CC Number", None))
        self.expDateLabel.setText(_translate("MainWindow", "Expiration Date", None))
        self.cvvLabel.setText(_translate("MainWindow", " CVV", None))
        self.typeLabel.setText(_translate("MainWindow", "Item Type", None))
        self.colorLabel.setText(_translate("MainWindow", "Color", None))
        self.keywordLabel.setText(_translate("MainWindow", "Keyword", None))
        self.sizeLabel.setText(_translate("MainWindow", "Item Size", None))
        self.sizeCombo.setItemText(0, _translate("MainWindow", "Small", None))
        self.sizeCombo.setItemText(1, _translate("MainWindow", "Medium", None))
        self.sizeCombo.setItemText(2, _translate("MainWindow", "Large", None))
        self.sizeCombo.setItemText(3, _translate("MainWindow", "XLarge", None))
        self.typeCombo.setItemText(0, _translate("MainWindow", "t-shirts", None))
        self.typeCombo.setItemText(1, _translate("MainWindow", "sweatshirts", None))
        self.typeCombo.setItemText(2, _translate("MainWindow", "tops/sweaters", None))
        self.typeCombo.setItemText(3, _translate("MainWindow", "shirts", None))
        self.typeCombo.setItemText(4, _translate("MainWindow", "jackets", None))
        self.typeCombo.setItemText(5, _translate("MainWindow", "hats", None))
        self.typeCombo.setItemText(6, _translate("MainWindow", "accessories", None))
        self.typeCombo.setItemText(7, _translate("MainWindow", "skate", None))
        self.typeCombo.setItemText(8, _translate("MainWindow", "shorts", None))
        self.typeCombo.setItemText(9, _translate("MainWindow", "pants", None))
        self.typeCombo.setItemText(10, _translate("MainWindow", "bags", None))
        self.typeCombo.setItemText(11, _translate("MainWindow", "shoes", None))
        self.delayLabel.setText(_translate("MainWindow", "Delay (s)", None))
        self.delayLineEdit.setText(_translate("MainWindow", "2", None))
        self.proxyLineEdit.setText(_translate("MainWindow", "IP:PORT", None))
        self.proxyLabel.setText(_translate("MainWindow", "Proxy", None))

    #IS PROGRAM RUNNING?
    running = False;

    '''
    LOAD all data from config file to GUI
    '''
    def loadButtonClick(self):

        # GET FILE
        path = QtGui.QFileDialog.getOpenFileName(self.setupUi(MainWindow), 'Load File')

        # LOAD FILE
        file = open(path, 'r')
        lines = file.readlines()

        #IS FILE IN CORRECT FORMAT?
        if "First:" in lines[0]:

            self.consoleEdit.append(self.UTCtoEST() + " [FILE LOADED] FILE HAS BEEN LOADED")

        else:

            self.consoleEdit.append(self.UTCtoEST() + " [FILE ERROR] FILE IS NOT IN CORRECT FORMAT")
            return

        # CLEANED DATA
        trueData = []

        #SPLIT DATA FROM DEFINITION
        for line in lines:

            data = line.split(sep=':')

            data[1] = data[1][0:-1]

            trueData.append(data[1])

        #WRITE DATA TO GUI
        self.firstNameEdit.setText(trueData[0])
        self.lastNameEdit.setText(trueData[1])
        self.emailLineEdit.setText(trueData[2])
        self.phoneLineEdit.setText(trueData[3])
        self.addressLineEdit.setText(trueData[4])
        self.zipLineEdit.setText(trueData[5])
        self.cityLineEdit.setText(trueData[6])
        self.countryCombo.setCurrentIndex(self.countryCombo.findText(trueData[7], QtCore.Qt.MatchFixedString))
        self.stateCombo.setCurrentIndex(self.stateCombo.findText(trueData[8], QtCore.Qt.MatchFixedString))
        self.cardCombobox.setCurrentIndex(self.cardCombobox.findText(trueData[9], QtCore.Qt.MatchFixedString))
        self.cardLineEdit.setText(trueData[10])
        self.expDateMonth.setValue(int(trueData[11]))
        self.expDateYear.setValue(int(trueData[12]))
        self.cvvLineEdit.setText(trueData[13])

    '''
    SAVE config file from inputs given from user
    '''
    def saveButtonClick(self):

        # GET DATA FILLED IN BY USER
        usrFirstName = 'First:' + str(self.firstNameEdit.text())
        usrLastName = 'Last:' + str(self.lastNameEdit.text())
        usrEmail = 'Email:' + str(self.emailLineEdit.text())
        usrPhone = 'Phone:' + str(self.phoneLineEdit.text())
        usrAddress = 'Address:' + str(self.addressLineEdit.text())
        usrZip = 'Zip:' + str(self.zipLineEdit.text())
        usrCity = 'City:' + str(self.cityLineEdit.text())
        usrCountry = 'Country:' + str(self.countryCombo.currentText())
        usrState = 'State:' + str(self.stateCombo.currentText())
        usrCardType = 'Type:' + str(self.cardCombobox.currentText())
        usrCardNumber = 'Number:' + str(self.cardLineEdit.text())
        usrExpMonth = 'Month:' + str(self.expDateMonth.cleanText())
        usrExpYear = 'Year:' + str(self.expDateYear.cleanText())
        usrCVV = 'CVV:' + str(self.cvvLineEdit.text())

        #SAVE FILE
        path = QtGui.QFileDialog.getSaveFileName()

        #OPEN FILE
        file = open(path, 'w')

        #WRITE DATA TO FILE
        file.write(usrFirstName + "\n")
        file.write(usrLastName + "\n")
        file.write(usrEmail + "\n")
        file.write(usrPhone + "\n")
        file.write(usrAddress + "\n")
        file.write(usrZip + "\n")
        file.write(usrCity + "\n")
        file.write(usrCountry + "\n")
        file.write(usrState + "\n")
        file.write(usrCardType + "\n")
        file.write(usrCardNumber + "\n")
        file.write(usrExpMonth + "\n")
        file.write(usrExpYear + "\n")
        file.write(usrCVV + "\n")

        #CLOSE FILE
        file.close()

    '''
    STOP running bot process
    '''
    def stopButtonClick(self):

        #IS PROGRAM RUNNING?
        if (self.running == True):

            self.consoleEdit.append(self.UTCtoEST() + " [STOPPED] PROCESS STOPPED.")

        self.running = False

    '''
    START bot and attempt to purchase item
    '''
    def startButtonClick(self):

        # WHEN START CLICKED PROGRAM IS RUNNING
        self.running = True

        ############### ITEM INFO ##################
        usrKeyword = str(self.keywordLineEdit.text())
        usrSize = str(self.sizeCombo.currentText())
        usrColor = str(self.colorLineEdit.text())
        usrType = str(self.typeCombo.currentText())
        usrDelay = int(self.delayLineEdit.text())

        ######### FIND ITEM IN SITE ################
        self.find(usrKeyword, usrColor, usrSize, usrType, usrDelay)

    '''
    CONVERT time
    @return current: Returns EST conversion from UTC
    '''
    def UTCtoEST(self):

        #Convert time to EST
        current = datetime.now()
        return str(current) + ' EST:'

    '''
    FIND item given by user on the Supreme web store
    This finds item, adds to cart then purchases item.
    '''
    def find(self, keyword, color, size, itemType, delay):

        #PROXY?

        # CHECK IF ALL SPACES IN ITEM TYPE IS FILLEDs
        if ((len(keyword) == 0) | (len(color) == 0)):
            self.consoleEdit.append(self.UTCtoEST() + ' [ERROR] DO NOT LEAVE ANY LINE EMPTY.')
            self.running = False
            return;

        # MAKE REQUESTS
        req = urllib_request.Request('http://www.supremenewyork.com/mobile_stock.json')
        req.add_header('User-Agent',
                       "User-Agent','Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_4 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B350 Safari/8536.25")

        # OPEN WEBSITE
        try:
            resp = urllib_request.urlopen(req, timeout=7)
        except (TimeoutError, URLError):
            # IS STOP NOT CLICKED?
            if (self.running == True):
                self.consoleEdit.append(self.UTCtoEST() + " [TIMED OUT] CHECK YOUR CONNECTION! RESTARTING CONNECTION!")
                QtGui.qApp.processEvents()
                self.find(keyword, color, size, itemType, delay)
            return;

        reader = codecs.getreader('utf-8')
        data = json.load(reader(resp))

        for i in range(len(data['products_and_categories'].values())):
            for j in range(len(list(data['products_and_categories'].values())[i])):
                # Set Item to variable
                try:
                    item = list(data['products_and_categories'].values())[i][j]
                except:
                    return;

                name = str(item['name'].encode('ascii', 'ignore')).lower()
                # SEARCH WORDS HERE
                # if string is in the name:
                in_name = True
                if not keyword.lower() in name:
                    in_name = False;
                elif in_name & (itemType == item['category_name'].lower()):
                    # match/(es) detected!
                    ID = str(item['id'])
                    # Another request being formed to get item.
                    jsonurl = 'http://www.supremenewyork.com/shop/' + str(ID) + '.json'
                    req = urllib_request.Request(jsonurl)
                    req.add_header('User-Agent',
                                   "User-Agent','Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_4 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B350 Safari/8536.25")

                    try:
                        resp = urllib_request.urlopen(req, timeout=5)
                    except (TimeoutError, URLError):
                        if (self.running == True):
                            self.consoleEdit.append(
                                self.UTCtoEST() + " [TIMED OUT] CHECK YOUR CONNECTION! RESTARTING CONNECTION!")
                            QtGui.qApp.processEvents()
                            self.find(keyword, color, size, itemType, delay)
                        return;

                    data = json.load(reader(resp))

                    for numCW in data['styles']:
                        # COLORWAY TERMS HERE
                        # if color in numCW['name']:
                        if color in numCW['name'].lower():

                            for sizes in numCW['sizes']:
                                # FIND THE SIZE
                                if str(sizes['name']) == size:
                                    variant = str(sizes['id'])
                                    cw = numCW['name']
                                    self.consoleEdit.append(
                                        self.UTCtoEST() + ' [ITEM SELECTED] ' + name[2:-1].upper() + ' ---- ' + sizes[
                                            'name'] + ' ---- ' + numCW['name'])
                                    # ADD TO CARD
                                    cart = self.add(ID, variant, cw)
                                    if (cart == True):
                                        self.checkout(ID, delay)
                                        return;

        # IF PROGRAM IS RUNNING AND ITEM HAS NOT BEEN FOUND YET
        try:
            if ((self.running == True)):
                time.sleep(.15)
                self.consoleEdit.append(self.UTCtoEST() + ' [RELOADING] Page is being reloaded until item is found...')  # incorrect keywords will cause this to loop forever
                QtGui.qApp.processEvents()
                self.find(keyword, color, size, itemType, delay)
        except:
            self.consoleEdit.append(self.UTCtoEST() + ' [WEBSITE TIMED OUT]')

    '''
    Add to cart the item given by user
    '''
    def add(self, ID, variant, cw):

        addUrl = 'http://www.supremenewyork.com/shop/' + ID + '/add.json'
        addHeaders = {
            'Host': 'www.supremenewyork.com',
            'Accept': 'application/json',
            'Proxy-Connection': 'keep-alive',
            'X-Requested-With': 'XMLHttpRequest',
            'Accept-Encoding': 'gzip, deflate',
            'Accept-Language': 'en-us',
            'Content-Type': 'application/x-www-form-urlencoded',
            'Origin': 'http://www.supremenewyork.com',
            'Connection': 'keep-alive',
            'User-Agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_1_2 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Mobile/11D257',
            'Referer': 'http://www.supremenewyork.com/mobile'
        }
        addPayload = {
            'size': variant,
            'qty': '1'
        }

        self.consoleEdit.append(self.UTCtoEST() + ' [ADDING ITEM TO CART]')

        # POST to addResp
        try:
            addResp = session.post(addUrl, data=addPayload, headers=addHeaders, timeout=7)
        except:
            self.consoleEdit.append(self.UTCtoEST() + ' [ERROR] ITEM COULD NOT BE ADDED.')
            self.consoleEdit.append(self.UTCtoEST() + ' [ERROR] TIMED OUT!.')
            self.running = False;
            return;

        # IS ITEM ADDED?
        try:
            if addResp.status_code != 200:
                self.consoleEdit.append(self.UTCtoEST() + ' [ERROR]', addResp.status_code, 'TIMEOUT!!!')
                self.running = False;
                return;
            else:
                try:
                    if addResp.json() == []:
                        self.consoleEdit.append(self.UTCtoEST() + ' [ERROR] ITEM COULD NOT BE ADDED.')
                        self.running = False;
                        return;
                except KeyError:
                    return;

            self.consoleEdit.append(self.UTCtoEST() + ' [ADDED] ITEM ADDED TO CART ')

            return True;
        except UnboundLocalError:
            self.consoleEdit.append(self.UTCtoEST() + ' [ERROR] ITEM COULD NOT BE ADDED.')
            self.running = False;
            return;

    '''
    CHECKOUT item given by user
    Delay depends on input given by user
    '''
    def checkout(self, ID, delay):

        ############### PERSONAL INFO ############
        usrfirstName = str(self.firstNameEdit.text())
        usrlastName = str(self.lastNameEdit.text())
        usrEMail = str(self.emailLineEdit.text())
        usrPhone = str(self.phoneLineEdit.text())
        usrAddress = str(self.addressLineEdit.text())
        usrZip = str(self.zipLineEdit.text())
        usrCity = str(self.cityLineEdit.text())
        usrState = str(self.stateCombo.currentText())
        usrCountry = str(self.countryCombo.currentText())
        usrExpDateMonth = str(self.expDateMonth.cleanText())
        usrExpDateYear = str(self.expDateYear.cleanText())
        usrCardNumber = str(self.cardLineEdit.text())
        usrCardType = str(self.cardCombobox.currentText())
        usrCardCvv = str(self.cvvLineEdit.text())

        #SETUP CARD TYPE FOR PAYLOAD
        if(usrCardType == "Mastercard"):
            usrCardType == 'master'
        elif(usrCardType == "Visa"):
            usrCardType == 'visa'

        ######### ADD ZERO TO MONTH IF LESS THAN 10 #############
        if int(usrExpDateMonth) < 10:
            usrExpDateMonth = str(0) + str(self.expDateMonth.cleanText())
        # PREPARE CHECKOUT
        cookie_sub = {}
        cookie_sub[ID] = 1
        cookie_sub = urllib_parse.quote(str(cookie_sub))

        checkoutUrl = 'https://www.supremenewyork.com/checkout.json'
        checkoutHeaders = {
            'host': 'www.supremenewyork.com',
            'If-None-Match': '"*"',
            'Accept': 'application/json',
            'Proxy-Connection': 'keep-alive',
            'Accept-Encoding': 'gzip, deflate',
            'Accept-Language': 'en-us',
            'Content-Type': 'application/x-www-form-urlencoded',
            'Origin': 'http://www.supremenewyork.com',
            'Connection': 'keep-alive',
            'User-Agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_1_2 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Mobile/11D257',
            'Referer': 'http://www.supremenewyork.com/mobile'
        }

        #################################
        # FILL OUT THESE FIELDS AS NEEDED
        #################################
        checkoutPayload = {
            'store_credit_id': '',
            'from_mobile': '1',
            'cookie-sub': cookie_sub,  # cookie-sub: eg. {"VARIANT":1} urlencoded
            'same_as_billing_address': '1',
            'order[billing_name]': usrfirstName + " " + usrlastName,  # FirstName LastName
            'order[email]': usrEMail,  # email@domain.com
            'order[tel]': usrPhone,  # phone-number-here
            'order[billing_address]': usrAddress,  # your address
            'order[billing_address_2]': '',
            'order[billing_zip]': usrZip,  # zip code
            'order[billing_city]': usrCity,  # city
            'order[billing_state]': usrState,  # state
            'order[billing_country]': usrCountry,  # country
            'store_address': '1',
            'credit_card[type]': usrCardType,  # master or visa
            'credit_card[cnb]': usrCardNumber,  # credit card number
            'credit_card[month]': usrExpDateMonth,  # expiration month
            'credit_card[year]': usrExpDateYear,  # expiration year
            'credit_card[vval]': usrCardCvv,  # cvc/cvv
            'order[terms]': '0',
            'order[terms]': '1'
        }

        self.consoleEdit.append(self.UTCtoEST() + ' [WAITING] ' + str(self.delayLineEdit.text()) + ' SECONDS BEFORE CHECKOUT!')

        # GHOST CHECKOUT PREVENTION WITH ROLLING PRINT
        for i in range(int(delay)):
            sys.stdout.flush()
            time.sleep(1)

        self.consoleEdit.append(self.UTCtoEST() + ' [CHECKING OUT] ')

        checkoutResp = session.post(checkoutUrl, data=checkoutPayload, headers=checkoutHeaders)

        try:
            self.consoleEdit.append(self.UTCtoEST() + ' [CHECKOUT] ' + checkoutResp.json()['status'].title() + '!')

        except KeyError:
            self.consoleEdit.append(self.UTCtoEST() + ' [ERROR] Reading status key of response!')
            self.running = False;

        except (TimeoutError, URLError):
            self.consoleEdit.append(self.UTCtoEST() + ' [ERROR] Failed to checkout!')
            self.running = False;

        if checkoutResp.json()['status'] == 'failed':
            try:
                print(checkoutResp.json())
                self.consoleEdit.append(self.UTCtoEST() + ' [ERROR] Check your information and try again.')
                self.running = False;

            except:
                self.consoleEdit.append(self.UTCtoEST() + ' [ERROR] Card was declined')
                self.running = False;


if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    ui = Ui_MainWindow()
    session = requests.Session()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())


